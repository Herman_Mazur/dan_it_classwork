"use strict";

// let vehicle = {
//     model: "BMW",
//     color: "Blue",
//     consumption: 7,

//     // start: function(){console.log("Engine started!")},
//     // stopEngine: function(){console.log("Engine stopped!")},
//     // getFuelConsumption: function(trip){
//     //     let consumption = 7 / 100;
//     //     return consumption * trip;
//     // }

//     start(){
//         console.log("Engine started!");
//     },

//     getFuelConsumption(trip){
//         return this.consumption / 100 * trip;
//     }
// }


// console.log(vehicle);
// console.log(vehicle.getFuelConsumption(400));


// ex 2

let mobilePhone = {
    brand: "Apple",
    storage: 512 * 1024,
    battery: 100,

    installApp(appName){
        console.log(`App ${appName} installed!`);
        this.storage -= 500;
        this.battery -= 4;
    },

    call(){
        console.log("Ringing...");
        this.battery -= 8;
    },

    chargePhone(){
        this.battery = 100;
    }


}

console.log(mobilePhone);

mobilePhone.installApp("Facebook");
mobilePhone.installApp("Instagram");
console.log(mobilePhone);
mobilePhone.chargePhone();
console.log(mobilePhone);
mobilePhone.call();
mobilePhone.call();
mobilePhone.call();

console.log(mobilePhone);
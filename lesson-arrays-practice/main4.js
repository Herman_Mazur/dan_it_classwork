let range = {
    from: 0,
    to: 0,

    setRange(from, to){
        this.from = from,
        this.to = to
    },

    [Symbol.iterator]: function(){
        return {
            start: this.from,
            end: this.to + 1,

            next(){
                if (this.start < this.end){
                    return {done: false, value: this.start++}
                }
                else {
                    return {done: true}
                }
            }


        }
    }
}

range.setRange(5, 10);

// for(let n of range){
//     console.log(n);
// }

let iter = range[Symbol.iterator]();

while(true){
    let info = iter.next();
    if (info.done){
        break
    }
    console.log(info.value)
}
let range = {
    from: 10,
    to: 20
}


range[Symbol.iterator] = function(){
    return {
        current: this.from,
        last: this.to,

        next(){
            if (this.current <= this.last){
                return {done: false, value: this.current++}
            }
            else {
                return {done: true}
            }
        }

    }
}

// for(let n of range){
//     console.log(n);
// }

let result = Array.from(range);
console.log(result)
console.log(Array.from("Hello world!"))
let products = {
    names: ['TV', 'Player', 'Phone', 'Laptop', 'Headphones'],
    prices: [200, 300, 100, 400, 50],

    addItem(name, price){
        this.names.push(name);
        this.prices.push(price);
    },

    [Symbol.iterator]: function(){
        return {
            items: this.names,
            prices: this.prices,
            currentIndex: 0,
            lastIndex: this.names.length,

            next(){
                if (this.currentIndex < this.lastIndex){
                    let data = {name: this.items[this.currentIndex], price: this.prices[this.currentIndex]};
                    this.currentIndex++;
                    return {done: false, value: data}
                }
                else {
                    return {done: true}
                }
            }

        }

    }

}


for (let p of products){
    console.log(p);
}

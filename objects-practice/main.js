// const CAR = {
//     name: 'BMW',
//     fuel: 70,
//     _status: false
// }

// Object.defineProperty(CAR, 'engineStatus', {
//     get(){
//         return this._status;
//     },

//     set(value) {
//         this._status = value;
//         if (value){
//             console.log("Engine started!")
//         }
//         else {
//             console.log('Engine stopped!')
//         }
//     }
// })


// Object.defineProperty(CAR, 'engineStatus', {
//     writable: false,
//     configurable: false
// })


// let testObj = {name: "Andrew"};

// Object.defineProperty(testObj, 'name', {
//     value: "Alex",
//     writable: false,
//     configurable:false
// });


// Object.defineProperty(testObj, 'age', {
//     value: 22,
//     writable: false,
//     configurable: false
// });

// console.log(testObj);


// let info = {
//     name: "Alex"
// }

// Object.defineProperty(info, 'name', {
//     get(){return this._name},
//     set(value){
//         console.log("Set new name!");
//         this._name = value
//     },
// })

// console.log(info);


// let data = {
//     _text: "Some text"
// }

// Object.defineProperty(data, 'text', {
//     set(val){
//         console.log("Writing data....");
//         this._text = val;
//     },

//     get(){
//         console.log("Data reading.......");
//         return this._text;
//     }
// })


// let itemInfo = {
//     product: "IPhone 14",
//     _price: 45000
// }


// Object.defineProperty(itemInfo, 'price', {
//     get(){
//         return `${this._price} UAH`
//     },

//     set(val){
//         if (val > 0){
//             this._price = val;
//         }
//     }
// })




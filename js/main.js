"use strict";
// // creation of variable
// let message = "Hello world fro js!";
// let firstname = "Maksim";
// // camecase style
// let testVariable = 1;
// // output information
// alert(message);

// let userName = "Maksim";
// alert(userName);

// // override
// userName = "Alex";
// alert(userName);

// const COLOR = "red";
// const BG_COLOR = "yellow";

// alert(COLOR);
// alert(BG_COLOR);

// ex1
// let admin;
// let name;
// name = "John";
// admin = name;
// alert(admin);

// let username = prompt("Enter your name:", "Default name...");
// alert(username);

// let result = confirm("Are you ready?");
// alert(result);

// let username = "Maksim123"; // string
// let age = 34.45; // number
// let status = true; // boolean

// let lastname; // undefined
// // alert(lastname);

// alert(typeof null);

// + - / *

// let a = 12;
// let b = 45;

// console.log(typeof a, typeof b);

// let perimetr = (a + b) * 2;

// alert(perimetr);

// let firstname = "Maksim";
// let lastname = "Fedorov";

// let fullName = firstname + " " + lastname;
// console.log(fullName);

// ex2

// const username = prompt("Enter your username:");
// console.log("Username:", username);
// alert("Hello, " + username);

// example confirm
// const confirmMessage = "Are you ready?";
// let answer = confirm(confirmMessage);
// console.log(answer);

// < > <= >= != ==, ===

// let check = 34 < 2;
// console.log(check);

// let firstNumber = Number(prompt("Enter first number:"));
// let secondNumber = Number(prompt("Enter second number:"));

// // let a = Number(firstNumber);
// // let b = Number(secondNumber);

// // console.log(typeof a, typeof b);

// // let result = a + b;
// let result = firstNumber + secondNumber;

// alert(result);

// examle

// alert(
//   (Number(prompt("Enter number 1")) + Number(prompt("Enter number 2"))) * 2
// );
